export const getIsolationCountry = (AgentsDate) => {
    let isolation = {};
    let isolationCountry = {};
    let mostIsolated = 0;
    AgentsDate.forEach(({agent, country}) =>{
        isolationCountry[country] =  isolationCountry[country] ? ++isolationCountry[country] : 1;
        isolation[agent] = isolation[agent] ? [...isolation[agent], country] : [country];
        if(isolation[agent].length > 1) {
            isolation[agent].forEach(country => {
                isolationCountry[country] = --isolationCountry[country];
            })
        }
    })
    mostIsolated = Math.max(...Object.values(isolationCountry));
    return Object.keys(isolationCountry).find((key) => isolationCountry[key] === mostIsolated);
}
