import React from 'react';
import style from './app.module.scss';
import { getIsolationCountry } from './utils/utilsFunctions';
import { AgentsDate } from './mock/agentsData';
import TopHeader from './components/header/header';
import AgentsTable from './components/agentsTable/agentsTable';

function App() {
   const isolatedCountry = getIsolationCountry(AgentsDate)
  return (
    <main className={style.AppContainer}>
      <TopHeader mostIsolated={isolatedCountry}/>
      <AgentsTable  agentsData={AgentsDate}/>
    </main>
  );
}

export default App;
