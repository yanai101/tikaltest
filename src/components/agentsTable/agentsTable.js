import React from "react";
import style from './agentsTable.module.scss';
import TableHeader from "../tableHeader/tableHeader";
import TableRow from "../tableRow/tableRow";
import TableFooter from "../tableFooter/tableFooter";

export default function AgentsTable({agentsData}) {

const agentDataSorted = agentsData.sort((a,b)=>{
    return new Date(b.date) - new Date(a.date);
});

  return (
    <div className={style.agentsTable}>
        <TableHeader/>
        {agentDataSorted.map(({agent,country,address,date},index) => (
            <TableRow agent={agent} country={country} address={address} date={date} key={index}/>
        ))}
        <TableFooter sumMissions={agentsData.length}/>
   </div>
  );
}