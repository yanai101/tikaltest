import React from "react";
import style from './header.module.scss';

export default function TopHeader({mostIsolated}) {
  return (
    <h4>Most isolated country : {mostIsolated}</h4>
  );
}
