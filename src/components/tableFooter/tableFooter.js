import React from "react";
import style from './tableFooter.module.scss';

export default function TableFooter({sumMissions}) {

  return (
    <div className={style.tableFooter}>
        {sumMissions} Missions
    </div>
  );
}