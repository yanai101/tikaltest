import React from "react";
import style from './tableRow.module.scss';

export default function TableRow({agent,country,address,date}) {

    return (
        <div className={style.tableRow}>
            <div>{agent}</div>
            <div>{country}</div>
            <div>{address}</div>
            <div>{date}</div>
        </div>
    );
}