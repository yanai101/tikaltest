import React from "react";
import style from './tableHeader.module.scss';

export default function TableHeader() {

  return (
    <div className={style.tableHeader}>
        <div>Agent ID</div>
        <div>Country</div>
        <div>Address</div>
        <div>Date</div>
    </div>
  );
}